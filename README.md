# Information
Game Title: Pixel Chess

Target Platform: Android\(version not specified\)

Art Style: 2D Pixel

Status: developing\(source code in the development branch as master branch is for the release\)

Target Platform for publishing: not specified, maybe itch.io then google play store

## Description
This project is just a practice for building a game on my own and revise how to use Unity. You may feel free to download the source code, but it is recommended for educational use and reference only.

## Features
- setting colors for the boards and pieces.
- changing background Music
- able to versus a simple game Android
- able to battle on network

# Credits
Music: [Eric Matyas](www.soundimage.org)

Graphics: [Devil's Work.shop](http://devilswork.shop/)